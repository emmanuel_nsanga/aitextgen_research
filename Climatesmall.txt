This website uses cookies and non-individually-identifiable tracking to help us understand and improve it. Learn more
Got it!
Want to talk about mine reclamation? Join us for Open Call on Feb. 8 and then every Tuesday until Mar. 22. Click here for call-in details.

Public Lab


Renegotiating Expertise: a talk at ClimateX MIT
by warren | 18 Jan 22:06 ...
I gave a talk today at MIT's ClimateX course, which is open to the public.

The speaker notes are a bit incoherent, but I hope to reformat them into a blog post or two. I'll include them here, but apologies if they're not very readable:


Renegotiating Expertise

Jeff Warren | Public Lab

is a community and a non-profit, democratizing science to address environmental issues that affect people.

We do what we call “Community Science” -- which involves supporting community knowledge production -- creating bridges & shared spaces between formal expertise and community needs.

In picture above,

I think folks often misunderstand what Public Lab is -- a friend once told me that it’s great that we’re helping the public to understand science. But that’s not it.

3

Public Lab is different because of our focus on the question of who, not what. We’re not teaching people about science, we’re trying to negotiate a new relationship between science parctice and the public. A more equitable and mutually beneficial relationship.

In our work to support communities facing pollution, Public Lab calls into question a lot of how expertise works today.

Who builds knowledge? Who is it for? Who asks the questions, and understands the answers?

4

We not only seek to make science’s findings accessible, but its methods, its tools, and who actually participates, and at what level.

This means both making more accessible “on-ramps” but also challenging what’s possible by leveraging peer production, open source, and the maker movement.

5

This includes balloon mapping, our oldest project...

6

And it often focuses on specific pollution sites that people want to investigate, where they live.

7

We also focus on making your own tools.

Our DIY Spectrometry project, our biggest.

8

People post their work at Public Lab to share it with others, and to ask for help.

These people may be scientists, but they’re just as likely to be educators, hobbyists, and the group we’re most interested in serving are those community groups who experience environmental problems first-hand.

9

Why Do It Yourself?

Why go beyond dissemination? In some ways, it’s because experts often have too narrow a conception of where the public could become involved. “Public dissemination of science” -- or data entry.

And of course, the cost barrier plays a role in Public Lab’s work -- cheap instrumentation democratizes knowledge production.

But to really answer that question, I think we need to take a few steps back and try to better understand how shared knowledge is produced -- and how expertise works.

10

Take a look at how expertise works today? How are projections or predictions made, why and when do people trust them?

http://www.nytimes.com/interactive/2016/upshot/presidential-polls-forecast.html

11

Data and its interpretation increasingly drives decision making in our society. Important because...

& you can see how this could become a problem where it displaces the discursive mode of debate which is the foundation of our democracy,

not only because of possible biases -- bought science, or ideological issues.

But because of the most subjective parts of science -- the selection of problems and questions, and of course, the application of science’s findings.

So it’s concerning when people lose trust.

I found it interesting that FiveThirtyEight and other data-driven analysts are increasingly tuning their language about certainty.

“More probable than making a field goal” - not helpful for people like me who know nothing of football...

http://www.nytimes.com/interactive/2016/upshot/presidential-polls-forecast.html

13

This kind of thing reflects on other narratives.

Now, I think it’s too easy to demonize experts when things go wrong. Much of complex knowledge is conveyed in rich, interactive modes (Bloomberg)

https://www.bloomberg.com/graphics/2015-whats-warming-the-world/

14

With such a wealth of data, and such persuasive communication of that data,

what’s broken about expertise? What we're all afraid of -- that climate science is ignored due to what Harry Collins calls

"downward discrimination" (?) expertise -- meta expertise, or mistrusting of expertise

What do we do about the widening gap between scientists and the public?

Public Lab: focus on problem definition * staying close to the real-world problems, and the people who know most about them

Collab as much in asking questions as in answering them

16

But really…

17

Limited ability to evaluate or test

Processes too big to see feedback loop in individual actions

18

This is one reason we focus on test kitability at PL. On building knowledge up from small, modular parts.

Can you build this? Do you get the same result?

Of course, this is challenging in climate science.

19

Environmental issues “affect someone else”

Already being lied to and hurt by industries and sometimes the scientists they employ

Other problems include the idea...

Story you might find surprising: Doing science on scientists

Testing the testers -- bucket brigade

20

So you might ask yourself…
