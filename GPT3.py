import os

from aitextgen import aitextgen
from aitextgen.TokenDataset import TokenDataset, merge_datasets
from aitextgen.utils import build_gpt2_config
from aitextgen.tokenizers import train_tokenizer

class Model:
    def __init__(self):
        self.params = 175*10**9
        gpt2_layers = 25*10**6
        Layers = params/gpt2_layers
        self.current_path = path = os.path.realpath(__file__)


    def train(self):
        file_name = 'Dlab_environmentalScience.txt'
        train_tokenizer(file_name)
        config = build_gpt2_config(vocab_size=5000, max_length=32, dropout=0.0, n_embd=256, n_layer=8, n_head=8)

        ai = aitextgen(config=config,
                    tokenizer_file="aitextgen.tokenizer.json",
                    to_gpu=True)

        for layer in range(self.Layers):                    
            ai.train(file_name,
                    line_by_line=False,
                    from_cache=False,
                    num_steps=5000,
                    generate_every=1000,
                    save_every=1000,
                    save_gdrive=False,
                    learning_rate=1e-3,
                    batch_size=256,

                    )
            ai.save(self.current_path+str(layer))       


        def test('test')
